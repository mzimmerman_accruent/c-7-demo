﻿using System;

namespace c_sharp_7.CSharp7
{
    public static class Tuples
    {
        public static void DoThemTuples()
        {
            // Old way
            Tuple<string, string, int, double> tuple =
                Tuple.Create("adam", "91281821JASJHDAHssh2#h4H#H@#Hh2h3H#H", 4, 6.5234);
            Console.WriteLine($"Username = {tuple.Item1}");
            Console.ReadLine();










            // new (,) notation
            ValueTuple<string, string> tuple2 = ("adam", "91281821JASJHDAHssh2#h4H#H@#Hh2h3H#H");
            Console.WriteLine($"Username = {tuple2.Item1}");
            Console.ReadLine();










            // named items
            (string userName, string password) theTup = ("adam", "91281821JASJHDAHssh2#h4H#H@#Hh2h3H#H");
            Console.WriteLine($"Username = {theTup.userName}");
            theTup = ("melissa","6541#shkm#ouh@98s7#DJ8Een8sd#H");
            Console.WriteLine($"Username = {theTup.userName}");
            Console.ReadLine();








            // unnamed tuple
            (int p, int q, int r) = (2, 3, 4);
            Console.WriteLine($"p = {p}");
            p = 8;
            Console.WriteLine($"p = {p}");
            Console.ReadLine();








            // how to get item names on existing value tuples
            var adam = ("adam", "m", "tuliper");
            (string firstName, string middleName, string lastName) cestMoi = adam;
            Console.WriteLine($"firstName = {cestMoi.firstName}");
            Console.ReadLine();










            // another way to name tuple items
            var speedAndHealth = (health: 100, speed: 10);
            Console.WriteLine($"health = {speedAndHealth.health}");
            Console.ReadLine();













            // can still use Item1, Item2, etc.
            var item = (num: 1, count: 2, name: "hello");
            item.Item1 = item.Item2 * item.Item1;








            var task = Tupler.ProcessLanguage();
            task.Wait();

            var result = task.Result;
            Console.WriteLine($"{result.keyPhrases}\r\n{result.sentiment}\r\n{result.language}");
            Console.ReadLine();
        }
    }
}