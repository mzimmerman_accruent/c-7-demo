﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace c_sharp_7.CSharp7
{
    public class GeneralizedAsync
    {
        private DateTime _lastUpdate;
        private decimal _lastQuote;

        public async Task<decimal> GetStockQuoteA()
        {
            //Here we ALWAYS return a full task object.
            //Requires heap allocation b/c Task<> is a class
            //Takes 120ns with JIT
            var quote = await new HttpClient().GetStringAsync("http://quotes");
            return decimal.Parse(quote);
        }






        public async ValueTask<decimal> GetStockQuoteB()
        {
            //No heap allocation if the result is known synchronously (here it is) b/c ValueTask<> is struct
            //Takes 65ns with JIT
            if (DateTime.Now.Subtract(_lastUpdate).TotalMilliseconds < 2000)
            {
                return _lastQuote;
            }
            else
            {
                _lastUpdate = DateTime.Now;
                var quote = await new HttpClient().GetStringAsync("http://quotes");
                _lastQuote = decimal.Parse(quote);
                return _lastQuote;
            }

        }
    }
}
