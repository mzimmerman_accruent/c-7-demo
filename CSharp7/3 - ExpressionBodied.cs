﻿using System;
using System.Collections.Generic;

namespace c_sharp_7.CSharp7
{

    /// <summary>
    /// We can now use accessors, constructors, finalizers
    /// </summary>
    public class OrderProcessor
    {
        private readonly Dictionary<Guid, Order> _orders = new Dictionary<Guid, Order>();

        // introduced in C#6
        public Dictionary<Guid, Order> Orders => _orders;



        //Expression bodied constructor
        public OrderProcessor(List<Order> orders) => orders.ForEach(o => _orders.Add(o.Key, o));


        //Destructor / finalizer can support it as well
        ~OrderProcessor() => CleanUpUnmanagedResources();





        private void CleanUpUnmanagedResources()
        {
            //File handles, connections, etc
        }
    }

    public class Order
    {
        private DateTime _orderDate;

        private Guid _key;

        public DateTime OrderDate
        {
            get => _orderDate;
            set => _orderDate = EnsureValidDateRange(value);
        }

        public Guid Key
        {
            get => _key;
            private set => _key = value;
        }

        public Order()
        {
            Key = Guid.NewGuid();
            _orderDate = DateTime.Now;
        }

        private DateTime EnsureValidDateRange(DateTime value)
        {
            //assume logic to clamp date range.
            return value;
        }
    }
}