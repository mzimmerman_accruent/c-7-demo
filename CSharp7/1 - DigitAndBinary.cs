﻿using System;

namespace c_sharp_7.CSharp7
{
    public class DigitAndBinary
    {
        public static void Process()
        {
            // _ as digit separator
            long universeAge = 13082000001;
            long age_of_universe = 13_082_000_001;
            int blah = 1_365_211_624;
            decimal gf = 1_342.84M;
            double asdf = 33_765.235;

            Console.WriteLine("Digit separator");
            Console.WriteLine($"13_082_000_001 becomes {age_of_universe}");
            Console.ReadLine();

            // Binary literal (with separator)
            Int32 bytes = 0b00000000_00000000_00000000_00101010;

            Console.WriteLine();
            Console.WriteLine("Binary literal");
            Console.WriteLine($"0b00000000_00000000_00000000_00101010 becomes {bytes}");
            Console.ReadLine();
        }
    }
}