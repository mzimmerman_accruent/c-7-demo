﻿using System;

namespace c_sharp_7.CSharp7
{
    public class Person
    {
        private string _name;

        public string Name
        {
            get => _name;

            // can use in null coalescing expression
            set => _name = value ?? throw new ArgumentNullException();
        }








        // constructor, combine with expression-bodied constructor syntax
        public Person(string name)
        {
            _name = name ?? throw new ArgumentNullException(name);
        }









        // can now use in ternary expressions
        public string GetFirstName()
        {
            var parts = Name.Split(' ');
            if (parts.Length > 0)
            {
                return parts[0];
            }
            else
            {
                throw new InvalidOperationException("No name!");
            }
        }












        // use for stubs, combine with expression-bodied syntax
        public string GetLastName()
        {
            throw new NotImplementedException();
        }
    }
}
