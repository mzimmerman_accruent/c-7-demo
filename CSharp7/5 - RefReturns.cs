﻿using System.Collections.Generic;

namespace c_sharp_7.CSharp7
{
    using System;

    //Imagine this is a large data structure with many smaller structs.
    //Want to access without repeated allocations
    public class GiganticData
    {
        public PlayerData PlayerData;
        public EnemyData EnemyData;
    }

    public struct PlayerData
    {
        public int Health;
        public int Coins;
        public PlayerInventory Inventory;
        public List<string> Tags;
        public List<PowerUp> Powerups;
        public long AntiCheatHash;
    }

    

    public class RefReturnsAndOuts
    {
        private GiganticData _data;

        public RefReturnsAndOuts()
        {
            _data = new GiganticData();
            _data.PlayerData.Health = 0x00110011;
            _data.EnemyData.Health = 0x00EE00EE;
        }

        public PlayerData GetPlayerDataNoRef()
        {
            //return a copy of the struct
            return _data.PlayerData;
        }



        public ref PlayerData GetPlayerData()
        {
            //return a ref to a item.
            return ref _data.PlayerData;
        }











        public void TestRefs()
        {
            // original value intact
            var playerDataNoRef = GetPlayerDataNoRef();

            Console.WriteLine($"Starting value: {playerDataNoRef.Health}");
            playerDataNoRef.Health = 3;
            Console.WriteLine($"Change value to: {playerDataNoRef.Health}");
            var playerDataNoRef2 = GetPlayerDataNoRef();
            Console.WriteLine($"Value from second object: {playerDataNoRef2.Health}");
            Console.ReadLine();

            // original value changed
            // note ref keyword in variable declaration
            ref PlayerData playerDataRef = ref GetPlayerData();

            Console.WriteLine($"Starting value: {playerDataRef.Health}");
            playerDataRef.Health = 10;
            Console.WriteLine($"Change value to: {playerDataRef.Health}");
            ref PlayerData playerDataRef2 = ref GetPlayerData();
            Console.WriteLine($"Value from second object: {playerDataRef2.Health}");
            Console.ReadLine();
        }
    }

    public struct PowerUp
    {

    }
    public struct PlayerInventory
    {

    }
    public struct EnemyData
    {
        public int Health;
    }
}
