﻿using System;

using static System.Console;

namespace c_sharp_7.CSharp7
{
    public interface IShape
    {
        double GetArea();
    }

    public class Circle : IShape
    {
        public double Radius { get; set; }

        public Circle(double radius) => Radius = radius;

        public double GetArea() => Math.PI * (Radius * Radius);
    }

    public class Rectangle : IShape
    {
        public double Length { get; set; }

        public double Height { get; set; }

        public Rectangle(double length, double height)
        {
            Length = length;
            Height = height;
        }

        public double GetArea() => Length * Height;
    }

    public struct Square
    {
        public double Side { get; }

        public Square(double side) => Side = side;
    }

    public class PatternMatching
    {
        public void GetTheArea(IShape shape)
        {
            //OldCasting(shape);
            NewCasting(shape);

            //OldConditional(shape);
            NewConditional(shape);
        }

        private static void OldCasting(IShape shape)
        {
            if (shape is Rectangle)
            {
                var rectangle = shape as Rectangle;
                var rectArea = rectangle.GetArea();
                // other rectangle-specific code
            }
            else if (shape is Circle)
            {
                var circle = shape as Circle;
                var circleArea = circle.GetArea();
                // other circle-specific code
            }
        }

        private static void NewCasting(IShape shape)
        {
            if (shape is Rectangle rect)
            {
                Console.WriteLine($"Rectangle with area of {rect.GetArea()}");
            }


            // compile error
            //rect.Height = 12;

            // switch no longer restricted to compile-time constants
            switch (shape)
            {
                case Circle c:
                    WriteLine($"Circle with radius {c.Radius}");
                    break;

                //not a shape!!
                //case Square s:

                // when condition
                case Rectangle s when s.Length == s.Height:
                    WriteLine($"Square with dimensions {s.Length} x {s.Height}");
                    break;

                case Rectangle r:
                    WriteLine($"Rectangle with dimensions {r.Length} x {r.Height}");
                    break;

                // default always run last
                default:
                    WriteLine("<unknown shape>");
                    break;
            }

            Console.ReadLine();
        }

        private static void OldConditional(IShape shape)
        {
            if (shape.GetArea() > 5)
            {
                var theArea = shape.GetArea();
                WriteLine($"Area is >5 {theArea}");
            }

            var area = shape.GetArea();
            if (area > 5)
            {
                WriteLine($"Area is >5 {area}");
            }
        }

        private static void NewConditional(IShape shape)
        {
            //Since is does variable 
            if (shape.GetArea() is double theArea && theArea > 5)
            {
                WriteLine($"Area is >5. Area = {theArea}");
            }

            // accessible outside if block
            theArea = 2;
            Console.ReadLine();
        }

        public void NullDemo(object shape)
        {
            // even though null can be converted to a shape, created so that 
            // null will never match any type pattern
            switch (shape)
            {
                case Circle c:
                    // can't be null so we don't need null checks here
                    WriteLine($"Circle with radius {c.Radius}");
                    break;
                case Rectangle r:
                    WriteLine($"Rectangle {r.Length} x {r.Height}");
                    break;
                case null:
                    WriteLine("null");
                    break;
                default:
                    WriteLine("<unknown shape>");
                    break;
            }

            Console.ReadLine();
        }





        // Putting concepts together
        public void PrintStars(object o)
        {
            Console.WriteLine(o);

            // old way
            if (o is int)
            {
                WriteLine(new string('*', (int)o));
            }

            if (o is string)
            {
                int counter;
                if (int.TryParse((string)o, out counter))
                {
                    WriteLine(new string('*', counter));
                }
            }







            //All in one
            if (o is int count || (o is string input && int.TryParse(input, out count)))
            {
                WriteLine(new string('*', count));
            }

            Console.ReadLine();
        }
    }
}