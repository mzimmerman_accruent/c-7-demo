﻿using System;

namespace c_sharp_7.CSharp7
{
    public static class LocalFunctions
    {
        public static int DoMath()
        {
            int Sum(int x, int y)
            {
                return x + y;
            }

            return Sum(10, 20);
        }
       











        // why use local function instead of delegate?
        public static int LocalFunctionFactorial(int n)
        {
            //can call before defined
            return nthFactorial(n);

            int nthFactorial(int number)
            {
                return (number < 2) ? 1 : number * nthFactorial(number - 1);
            }
        }



        public static int LambdaFactorial(int n)
        {
            //Must declare first, then call
            var nthFactorial = default(Func<int, int>);
            nthFactorial = 
                (number) => (number < 2) ? 1 : number * nthFactorial(number - 1);

            Func<int, int> invalidFunc = (number) => (number < 2) ? 1 : number * nthFactorial(number - 1);

            return nthFactorial(n);
        }







        public static int Fibonacci(int x)
        {
            if (x < 0) throw new ArgumentException("Less negativity please!", nameof(x));

            //Note this is a value on the tuple
            return Fib(x).current;

            (int current, int previous) Fib(int i)
            {
                if (i == 0)
                {
                    return (1, 0);
                }
                Console.WriteLine($"Calling with {i-1}");
                var (p, pp) = Fib(i - 1);
                Console.WriteLine((p + pp, p));
                return (p + pp, p);
            }
            
        }





        static public void QuickSort(int[] items, int left, int right)
        {
            // can use variable inside local function
            var counter = 0;

            if (left < right)
            {
                int pivot = Partition(items, left, right);

                if (pivot > 1)
                    QuickSort(items, left, pivot - 1);

                if (pivot + 1 < right)
                    QuickSort(items, pivot + 1, right);
            }

            int Partition(int[] numbers, int start, int end)
            {
                counter++;

                // not the same pivot variable declared outside of local function
                int pivot = numbers[start];
                while (true)
                {
                    while (numbers[start] < pivot)
                        start++;

                    while (numbers[end] > pivot)
                        end--;

                    if (start < end)
                    {
                        int temp = numbers[end];
                        numbers[end] = numbers[start];
                        numbers[start] = temp;
                    }
                    else
                    {
                        return end;
                    }
                }
            }
        }
    }
}