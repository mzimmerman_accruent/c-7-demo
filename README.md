#README.md

This repo is the source code used to demo new C# 7 features on August 31, 2017. It is based on a similar 
presentation given by Adam Tuliper at Visual Studio Live 2017 Austin. Adam's original source code can be 
viewed at https://github.com/adamtuliper/C-7-Features.

It is recommended that this solution be used in Visual Studio 2017 (aka I think you can use it in Visual 
Studio 2015, but I don't know how to configure that).

######Nuget Packages Needed
* System.Threading.Tasks.Extensions
* System.ValueTuple

######Changing language versions
Depending on which folders you are working in, you may need to manually change the project's language 
version. To do so, right-click the project name in the Solution Explorer and select Properties.
On the Build tab, scroll to the bottom and click the Advanced... button. Select the appropriate value from 
the Language version dropdown and click OK.