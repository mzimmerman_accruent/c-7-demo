﻿namespace c_sharp_7
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Speech.Synthesis;
    using System.Xml;

    using c_sharp_7.CSharp7;

    public class Program
    {

        static void Main(string[] args)
        {
            /******************************************************
               1. Binary Literal and digit separator
             ******************************************************/
            //DigitAndBinary.Process();
            //return;


            /******************************************************
              2. Out vars
            ******************************************************/
            //OutVars.Process();
            //return;


            /******************************************************
              3. Expression bodied
            ******************************************************/
            //List<Order> orders = new List<Order>() { new Order(), new Order(), new Order() };
            //var processor = new OrderProcessor(orders);
            //var firstOrder = processor.Orders.FirstOrDefault();
            //return;


            /******************************************************
              4. Throw expression
            ******************************************************/
            //try
            //{
            //    var person = new CSharp7.Person(null);
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine($"Caught throw expression: {ex.GetType()} - {ex.Message}");
            //}

            //Console.ReadLine();
            //return;


            /******************************************************
              5. Ref return
            ******************************************************/
            //var refReturns = new RefReturnsAndOuts();
            //refReturns.TestRefs();
            //return;


            /******************************************************
              6. Local functions
            ******************************************************/
            //Console.WriteLine(LocalFunctions.DoMath());
            //Console.ReadLine();

            //int[] numbers = { 3, 8, 7, 5, 2, 1, 9, 6, 4 };
            //LocalFunctions.QuickSort(numbers, 0, numbers.Length - 1);
            //Console.WriteLine("Sorted items");
            //numbers.ToList().ForEach(o => Console.WriteLine(o));
            //Console.ReadLine();
            //return;


            /******************************************************
              7. Pattern matching
            ******************************************************/
            //var pm = new PatternMatching();
            //pm.GetTheArea(new Circle(3.5d));
            //pm.GetTheArea(new Rectangle(2d, 2d));
            //pm.GetTheArea(new Rectangle(1d, 3d));

            //pm.NullDemo(null);

            //pm.PrintStars(5);
            //pm.PrintStars("5");
            //pm.PrintStars(new Rectangle(3, 4));
            //return;


            /******************************************************
              8. Value tuples
            ******************************************************/
            // have to install System.ValueTuple nuget package
            //Tuples.DoThemTuples();

            //8b. Deconstruction
            //var point = new Point(5, 4);
            //var (p1, p2) = point;
            //Console.WriteLine(p1);

            ////Deconstruction, I only want one item
            //(int a10, _, _, _, _) = (1, 2, 3, 4, 5);
            //var (x, _) = point;
            //Console.WriteLine(x);
            //Console.Read();


            /******************************************************
              8. GeneralizedAsyncReturn
            ******************************************************/

            //have to install System.Threading.Tasks.Extensions nuget package
            //var task = new GeneralizedAsync();
            //var quoteA = task.GetStockQuoteA();
            //var quoteB = task.GetStockQuoteB();


            /******************************************************
              Just for fun, nothing to do with C#7
            ******************************************************/
            //using (var synthesizer = new SpeechSynthesizer())
            //{
            //    var list = synthesizer.GetInstalledVoices();
            //    foreach (var v in list.Select(v => v.VoiceInfo))
            //    {
            //        synthesizer.SelectVoice(v.Name);
            //        synthesizer.Rate = 2;
            //        synthesizer.Speak("yo homie. what it is?");
            //    }
            //}
        }
    }
}
