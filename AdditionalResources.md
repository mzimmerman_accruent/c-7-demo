##C# 7 Further Reading
What's new in C# 7 on Microsoft Docs https://docs.microsoft.com/en-us/dotnet/csharp/whats-new/csharp-7
The future of C# presentation from Build 2017 https://channel9.msdn.com/Events/Build/2017/B8104
C# 7: First Look course on Pluralsight https://app.pluralsight.com/library/courses/csharp-7-first-look/table-of-contents


##Cool C#/.NET/Visual Studio Resources
.NET Framework Source Code http://referencesource.microsoft.com/
C# Language Design GitHub https://github.com/dotnet/csharplang
C# Learning Path on Pluralsight https://app.pluralsight.com/paths/skill/csharp
Microsoft Virtual Academy https://mva.microsoft.com/
Accruent Academy Engineering Channel